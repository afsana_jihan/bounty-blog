<?php
/*------------------------For User---------------------------*/
Route::get('/',[ 'as'=>'index', 'uses'=>'FrontController@index']);
Route::get('/profile','FrontController@showprofile')->name('profile');
Route::get('/userPost','FrontController@showuserPost')->name('userPost');
Route::get('/singlePost/{id}','FrontController@show')->name('singlePost');
Route::get('/categorywisepost/{id}','FrontController@showPostCategorywise')->name('categorywisepost');
Route:: get('/search','FrontController@search');
Auth::routes();
/*------------------------For Admin-------------------------*/
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('post','PostController');
//    Route:: get('/search','PostController@search');
    Route::post('post/changeStatus',['as'=>'post.changeStatus', 'uses'=>'PostController@changeStatus']);
    Route::get('post/allPostForAdmin',['as'=>'post.allPostForAdmin', 'uses'=>'PostController@allPostForAdmin']);
    Route::resource('category','CategoryController');
    Route::resource('gallery','GalleryController');
});




<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_title','category_id','user_id','post_description','post_slug', 'approved_status'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function gallery() {
        return $this->belongsToMany('App\Gallery'); //  or whatever your namespace is
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}

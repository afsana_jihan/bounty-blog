<?php

namespace App\Http\Controllers;

use App\Category;
use App\Gallery;
use App\Post;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['posts'] = Post::all();
        $data['categories'] = Category::all();

//        $posts = Post::with('category')->get();
//        return view('admin.post.index',compact('posts'));


        return view('user.post.userPost',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $categories = Category::get();
        return view('user.post.create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $this->validate($request,[
//            'post_title' => 'required',
//            'category_id' => 'required',
//            'post_description' =>'required'
//        ]);
//        $inputs = [];
       $postRequest = $request->only('user_id','post_title','category_id','post_description');
       $postRequest['post_slug'] = Str::slug($request->post_title,'-');
       $postRequest['approved_status'] =  $request->has('approved_status')? true:false;
        // $postRequest->approved_status;

       $post = Post::create($postRequest);




        // for multiple image upload


        $postGallery = [];
        $images = $request->file('path');

        $postGallery['post_id'] = $post->post_id;

        foreach($images as $image) {

//            $name= rand(11111, 99999) . '.' . $image['path']->getClientOriginalExtension();
//            $image['path']->move(public_path('/images/'), $name);
//            $postGallery['path'] = $name;
//            $postGallery['type'] = 'image';
//            Gallery::create($postGallery);

            $destinationPath = public_path('/images/'); // upload path
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);


            $postGallery['path'] = $profileImage;
            $postGallery['type'] = 'image';
            Gallery::create($postGallery);

        }
////        for video upload
//        $videos = $request->file('path');
//
//        foreach($videos as $video) {
//            $destinationPath = public_path('/videos/');
//
//            $request->file('path')->move(public_path('videos'), $request->file('path')->getClientOriginalName());
//            $video->path = public_path('videos') . '/' . $request->file('path')->getClientOriginalName();
//            $postGallery['path'] = $video;
//            $postGallery['type'] = 'video';
//            Gallery::create($postGallery);
//        }
//        for upload one picture only
//            $postGallery = $request->only('path', 'post_id');
//            $postGallery['post_id'] = $post->post_id;
//
//            $name = rand(11111, 99999) . '.' . $request->path->getClientOriginalExtension();
//            $postGallery['path']->move(public_path('/images/'), $name);
//            $postGallery['path'] = $name;
//            $postGallery['type'] = 'image';
//
//
//
//        Gallery::create($postGallery);



        return redirect()->route('post.index')->with('success','Post Submitted');

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $data['post'] = Post::query()->where('post_id','=',$id)->first();
        $data['posts'] = Post::get();
        $data['categories'] = Category::get();
        return view('admin.post.edit', $data);
    }


       public function update(Request $request, $id)
    {

        $this->validate($request,[
            'post_title' => 'required',
            'category_id' => 'required',
            'post_description' =>'required'
        ]);
        $post = Post::where('post_id', $id)->first();
        $inputs = [];
        $inputs = $request->all();
        $inputs['post_slug'] = Str::slug($request->post_title,'-');
//        $inputs['user_id'] = Auth::user()->id;

        $post->update($inputs);
        return redirect()->route('post.index')->with('success','Post updated');
    }


    public function destroy($id)
    {
        $post = Post::where('post_id',$id);
        $post->delete();
        return redirect()->route('post.index')->with('success','Post Deleted');

    }
     public function changeStatus(Request $request)
    {
        $post = Post::where('post_id', $request->post_id)->first();

        if($post->approved_status == 0)
            $inputs['approved_status'] = 1;
        else
            $inputs['approved_status'] = 0;

        $post->update($inputs);
        //return response()->json(['success'=>'Approved status change successfully.']);
    }
    public function allPostForAdmin()
    {
        $data['posts'] = Post::all();
        $data['categories'] = Category::all();

        return view('admin.post.index',$data);
    }
}

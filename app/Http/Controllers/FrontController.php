<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Auth;


class FrontController extends Controller
{
    public function __construct()
    {

    }


    public function index()
    {
//        $data['posts']=Gallery::with('post')->get();
        $data['posts'] = Post::all()->where('approved_status','=','1');

        $data = $this->setNavigationMenu($data);

        return view('user.layouts.app',$data);

    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {

        $data['post'] = Post::where('post_id','=',$id)->first();

        $data['posts'] = Post::get();

        $data = $this->setNavigationMenu($data);

        return view('user.post.singlePost',$data);

    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function showprofile()
    {
        $data['id'] = User::query()->where('id', '=',Auth::user()->id)->first();
        $data['posts'] = Post::get();
        return view('user.layouts.profile',$data);
    }

    public function showUserPost()
    {
        $data['user_id'] = User::query()->where('id','=',Auth::user()->id)->first();
        $data['posts'] = Post::get();
        return view('user.post.userPost',$data);
    }

    public function showPostCategorywise($id){

        $data['category'] = Category::query()->where('category_id','=',$id)->first();
        $sub_category = Category::where('parent_id','=',$id)->get()->pluck('category_id');

        $posts = Post::where('category_id','=',$id);

        if( count($sub_category)>0 )
        {
            $posts = $posts->orWhereIn('category_id', $sub_category);
        }

        $data['posts']= $posts->get()->where('approved_status', 1);

//        dd($data['posts']->toSql());
        $data = $this->setNavigationMenu($data);
        return view('user.post.categorywisePost',$data);
    }

    public function search(Request $request){

        $data['search'] = $request->get('search');
        $data['posts'] = Post::query()->where('post_title','Like',"%{$data['search']}%")->get();

        $data = $this->setNavigationMenu($data);
        return view('user.layouts.app',$data);
    }


    private function setNavigationMenu($data)
    {
        $data['categories'] = Category::where('parent_id', null)->get()->toArray();
        foreach($data['categories']  as $key=>$value)
        {
            $data['categories'][$key]['subcategory'] = Category::where('parent_id','=', $value['category_id'])->get()->toArray();
        }
        return $data;
    }


}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


    public function index()
    {
        $data['categories'] = Category::get();
        $data['parent_id'] = Category::get('parent_id');
        return view('admin.category.index',$data);
    }

    public function create()
    {
        $categories = Category::get();
        return view('admin.category.create',compact('categories'));
    }


    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'category_name' => 'required'
        ]);
        $inputs = [];
        $inputs = $request->all();
        $inputs['slug'] = Str::slug($request->category_name,'-');
        $category = Category::create($inputs);
        return redirect()->route('category.index')->with('success','Category added');
    }


    public function show($id)
    {
//        $data['posts'] = Post::all()->where('category_id','=',$id);
//
//        return view('user.post.categorywisePost',$data);
    }

    public function edit($id)
    {
        $data['category'] = Category::query()->where('category_id','=',$id)->first();
        $data['categories'] = Category::get();
        return view('admin.category.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category_name' => 'required'
        ]);
        $category = Category::where('category_id', $id)->first();
        $inputs = [];
        $inputs = $request->all();
        $inputs['slug'] = Str::slug($request->category_name,'-');

         $category->update($inputs);
        return redirect()->route('category.index')->with('success','Category Updated');
    }


    public function destroy($id)
    {
        $category = Category::where('category_id',$id);
        $category->delete();
        return redirect()->route('category.index')->with('success','Category Deleted');
    }

}

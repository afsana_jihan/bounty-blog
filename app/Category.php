<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    protected $fillable = ['category_name','parent_id','status','slug'];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'category_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    protected $table = 'post_galleries';
    protected $primaryKey = 'id';
    protected $fillable = ['post_id','path','type'];


    public function post() {
        return $this->belongsTo('App\Post'); //  or whatever your namespace is
    }
}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 04:23:10 GMT -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Bounty Blog</title>
    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('admin/images/favicon.ico') }}" type="image/x-icon') }}">
    <!-- Bootstrap core CSS-->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{ URL::asset('admin/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{ URL::asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Custom Style-->
    <link href="{{ URL::asset('admin/css/app-style.css') }}" rel="stylesheet"/>

</head>

<body class="authentication-bg">
<!-- Start wrapper-->
<div id="wrapper">
    <div class="card card-authentication1 mx-auto my-5 animated zoomIn">
        <div class="card-body">
            <div class="card-content p-2">
                <div class="text-center">
                    <img src="{{ URL::asset('admin/images/logo-icon.png') }}"/>
                </div>
                <div class="card-title text-uppercase text-center py-2">Login</div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <div class="position-relative has-icon-left">
                            <label for="exampleInputUsername" class="sr-only">{{ __('E-Mail Address') }}</label>
                            <input id="email"  class="form-control" id="exampleInputUsername" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="position-relative has-icon-left">
                            <label for="exampleInputPassword" class="sr-only">{{ __('Password') }}</label>

                            <input id="password"  placeholder="Password" id="exampleInputPassword" class="form-control" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <div class="form-control-position">
                                <i class="icon-lock"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mr-0 ml-0">
                        <div class="form-group col-6">
                            <div class="icheck-material-primary">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>


                                <label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
                            </div>
                        </div>
                        <div class="form-group col-6 text-right">
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-danger shadow-danger btn-block waves-effect waves-light">{{ __('Login') }}</button>
                    </div>
                    <div class="form-group text-center">
                        <p class="text-muted">Not a Member ? <a href="{{route('register')}}">Register here</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
</div><!--wrapper-->


<!-- Bootstrap core JavaScript-->
<script src="{{ URL::asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
<script src="{{ URL::asset('adminassets/js/bootstrap.min.js') }}"></script>
<!-- waves effect js -->
<script src="{{ URL::asset('admin/js/waves.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ URL::asset('admin/js/app-script.js') }}"></script>

</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/authentication-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 04:23:10 GMT -->
</html>

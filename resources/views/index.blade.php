@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <br />
            <a href="/post" style ="text-decoration: none"> <h3 align="center">All Post</h3> </a>
        </div>
        <br />
        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{$message}}</p>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-2">

        </div>

        <div class="col-md-4 text-right">
            <a href="{{route('post.home')}}" class="btn btn-primary">Add</a>
            <br />
            <br />
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <tr>
            <th>SL No.</th>
            <th>User Id</th>
            <th>Post Title</th>
            <th>Post Description</th>
        </tr>

        @foreach($posts as $row)
            <tr>
                <td>{{$row['post_id']}}</td>
                <td>{{$row['user_id']}}</td>
                <td>{{$row['post_title']}}</td>
                <td>{{$row['post_description']}}</td>
            </tr>
        @endforeach
    </table>

@endsection

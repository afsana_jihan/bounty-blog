<!DOCTYPE html>
<html lang="en">
<title>User All Post</title>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('admin/images/favicon.ico') }}" type="image/x-icon">
    <!-- notifications css -->
    <link rel="stylesheet" href="{{ URL::asset('admin/plugins/notifications/css/lobibox.min.css') }}"/>
    <!-- Vector CSS -->
    <link href="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>
    <!-- simplebar CSS-->
    <link href="{{ URL::asset('admin/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{ URL::asset('admin/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{ URL::asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{ URL::asset('admin/css/sidebar-menu.css') }}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{ URL::asset('admin/css/app-style.css') }}" rel="stylesheet"/>

</head>


<body>

<div class="container-fluid">
    <!-- Breadcrumb-->
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Add Post</h4>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route('post.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="basic-input" class="col-sm-3 col-form-label">Post Title</label>
                            <div class="col-sm-9">
                                <input type="text" id="basic-input"  name="post_title" class="form-control">
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="form-group row">
                            <label for="basic-select" class="col-sm-3 col-form-label">Select Post Category</label>
                            <div class="col-sm-9">
                                <select  class="form-control" name="category_id">
                                    @foreach($categories as $row)
                                        <option value="{{$row['category_id']}}">{{$row['category_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="basic-textarea" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea rows="10" class="form-control" name="post_description" id="basic-textarea"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="basic-textarea" class="col-sm-3 col-form-label">Post Image</label>
                            <div class="col-sm-9">
                                <input type="file" name="path[]" multiple>
                            </div>
                        </div>
                        {{--                            <div class="form-group row">--}}
                        {{--                                <label for="basic-textarea" class="col-sm-3 col-form-label">Post Video</label>--}}
                        {{--                                <div class="col-sm-9">--}}
                        {{--                                    <input type="text" name="path[]" multiple>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}


                        <div class="form-group row">
                            <div class="col-offset-md-9 col-sm-3">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--End Row-->
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ URL::asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>

<!-- simplebar js -->
<script src="{{ URL::asset('admin/plugins/simplebar/js/simplebar.js') }}"></script>
<!-- waves effect js -->
<script src="{{ URL::asset('admin/js/waves.js') }}"></script>
<!-- sidebar-menu js -->
<script src="{{ URL::asset('admin/js/sidebar-menu.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ URL::asset('admin/js/app-script.js') }}"></script>

<!-- Vector map JavaScript -->
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- Sparkline JS -->
<script src="{{ URL::asset('admin/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
<!-- Chart js -->
<script src="{{ URL::asset('admin/plugins/Chart.js/Chart.min.js') }}"></script>
<!--notification js -->
<script src="{{ URL::asset('admin/plugins/notifications/js/lobibox.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/notifications/js/notifications.min.js') }}"></script>
<!-- Index js -->
<script src="{{ URL::asset('admin/js/index.js') }}"></script>
@stack('scripts')


</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 03:56:12 GMT -->
</html>

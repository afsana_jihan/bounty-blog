@extends('user.layouts.layout')
@section('content')
<!-- Start Post Silder Area -->
<section class="post-slider-area">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="owl-carousel active-post-carusel">
                    <!-- single carousel item -->
                    <div class="single-post-carousel">
                        <div class="post-thumb">
                            <img class="img-fluid" src="{{asset('user/img/posts/carousel/post1.jpg')}}" alt="">
                        </div>

                    </div>
                    <!-- single carousel item -->
                    <div class="single-post-carousel">
                        <div class="post-thumb">
                            <img class="img-fluid" src="{{asset('user/img/posts/carousel/post2.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!-- Start Post Silder Area -->

<!-- Start main body Area -->
<div class="main-body section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 post-list">
                <!-- Start Post Area -->
                <section class="post-area">
                    <div class="row">
                        @foreach($posts as $row)
                            <div class="col-lg-4 col-md-4">
                                <div class="single-post-item">
                                    <div class="post-thumb">
                                        {{--<img class="img-fluid" src="{{asset('images/' .$row->path)}}" alt="">--}}
                                        <img class="img-fluid" src="{{asset('user/img/archive/c2.jpg')}}" alt="">
                                    </div>
                                    <div class="post-details">
                                        <h4><a href="{{route('singlePost',$row->post_id)}}">{{$row->post_title}}</a></h4>
                                        <p>{{$row->post_description}}</p>
                                        <div class="blog-meta">
                                            <a href="#" class="m-gap"><span class="lnr lnr-calendar-full"></span>{{$row->created_at}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>

                </section>
                <!-- Start Post Area -->
            </div>


        </div>
    </div>
</div>
@endsection

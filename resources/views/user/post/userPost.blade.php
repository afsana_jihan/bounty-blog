<!DOCTYPE html>
<html lang="en">
<title>User All Post</title>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('admin/images/favicon.ico') }}" type="image/x-icon">
    <!-- notifications css -->
    <link rel="stylesheet" href="{{ URL::asset('admin/plugins/notifications/css/lobibox.min.css') }}"/>
    <!-- Vector CSS -->
    <link href="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>
    <!-- simplebar CSS-->
    <link href="{{ URL::asset('admin/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{ URL::asset('admin/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{ URL::asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{ URL::asset('admin/css/sidebar-menu.css') }}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{ URL::asset('admin/css/app-style.css') }}" rel="stylesheet"/>

</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-table"></i> All Post</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Post Title</th>
                                <th>Post Category</th>
                                <th>Post Description</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $row)
                                @if($row->user_id==Auth::user()->id)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$row->post_title}}</td>
                                <td>{{$row->category->category_name}}</td>
                                <td>{{$row->post_description}}</td>
                                <th><button typ="submit" class="btn btn-success">Edit</button></th>
                            </tr>
                            @endif
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

<!-- End footer Area -->

<!-- Bootstrap core JavaScript-->
<script src="{{ URL::asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>

<!-- simplebar js -->
<script src="{{ URL::asset('admin/plugins/simplebar/js/simplebar.js') }}"></script>
<!-- waves effect js -->
<script src="{{ URL::asset('admin/js/waves.js') }}"></script>
<!-- sidebar-menu js -->
<script src="{{ URL::asset('admin/js/sidebar-menu.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ URL::asset('admin/js/app-script.js') }}"></script>

<!-- Vector map JavaScript -->
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- Sparkline JS -->
<script src="{{ URL::asset('admin/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
<!-- Chart js -->
<script src="{{ URL::asset('admin/plugins/Chart.js/Chart.min.js') }}"></script>
<!--notification js -->
<script src="{{ URL::asset('admin/plugins/notifications/js/lobibox.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/notifications/js/notifications.min.js') }}"></script>
<!-- Index js -->
<script src="{{ URL::asset('admin/js/index.js') }}"></script>
@stack('scripts')


</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 03:56:12 GMT -->
</html>

<li class="menu-active"><a href="index.html">Home</a></li>
@foreach($categories as $key => $row )
    @php
        //$child = app::make("FrontController")->ischild($row->category_id);
//                  $child =App\Http\Controllers\FrontController::ischild($row->category_id);

$count = isset($row['subcategory']) ? count($row['subcategory']) : 0 ;
    @endphp
    <li> <a  class="{{$count>0 ? 'menu-has-children': ''}}" href=" {{route('categorywisepost',$row['category_id'])}}">
            {{$row['category_name']}}</a>
        @if($count>0)
            <ul>
                @if(isset($row['subcategory']))
                    @foreach( $row['subcategory'] as $value)
                        <li>
                            <a href="{{route('categorywisepost',$value['category_id'])}}">{{ $value['category_name'] }}</a>
                        </li>
                    @endforeach
                @endif
            </ul>
        @endif
    </li>
@endforeach

                    <li><a href="contact.html">Contact</a></li>
@if(Auth::user())
                    <li><a>Profile Setting</a>
                    <ul>
                    <li><a href="{{ route('profile') }}">Profile</a> </li>

                            <li><a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
@endif

@if(!Auth::user())
                    <li><a href="{{ route('login') }}">Login</a></li>
                        @if (Route::has('register'))
                    <li><a href="{{ route('register') }}">Register</a></li>
                        @endif
    @endif

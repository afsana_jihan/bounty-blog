<header id="header">
    <div class="container box_1170 main-menu">
        <div class="row align-items-center justify-content-center d-flex">
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    @include('user.includes.navbar')
                </ul>
            </nav>
        </div>
    </div>
    @include('user.includes.searchbox')
</header>

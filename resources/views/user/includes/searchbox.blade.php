<div class="search_input" id="search_input_box">
    <div class="container box_1170">
        <form class="d-flex justify-content-between" action="/search" method="get">
            @csrf
            <input type="search"  name="search" class="form-control" id="search_input" placeholder="Search Here">
            <button type="submit" class="btn"></button>
            <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
        </form>
    </div>
</div>

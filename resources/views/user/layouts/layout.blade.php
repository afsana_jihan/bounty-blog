<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('user/img/fav.png') }}">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Ruft Blog</title>

    <link href="{{url('https://fonts.googleapis.com/css?family=Roboto:400,500|Rubik:500')}}" rel="stylesheet">
    <!--================================CSS ============================================= -->
    <link rel="stylesheet" href="{{ asset('user/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
</head>

<body>
<!--================ Start header Top Area =================-->
<section class="header-top">
    <div class="container box_1170">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <a href="index.html" class="logo">
                    <img src="{{ asset('user/img/logo.png') }}" alt="">
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 search-trigger">
                <a href="#" class="search">
                    <i class="lnr lnr-magnifier" id="search"></i></a>
                </a>
            </div>
        </div>
    </div>
</section>
<!--================ End header top Area =================-->

@include('user.includes.header')

@yield('content')

@include('user.includes.footer')

<script src="{{ asset('user/js/vendor/jquery-2.2.4.min.js') }}"></script>
<script src="{{url('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js')}}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="{{ asset('user/js/vendor/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{ asset('user/js/easing.min.js') }}"></script>
<script src="{{ asset('user/js/hoverIntent.js') }}"></script>
<script src="{{ asset('user/js/superfish.min.js') }}"></script>
<script src="{{ asset('user/js/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('user/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('user/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('user/js/jquery.tabs.min.js') }}"></script>
<script src="{{ asset('user/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('user/js/waypoints.min.js') }}"></script>
<script src="{{ asset('user/js/mail-script.js') }}"></script>
<script src="{{ asset('user/js/main.js') }}"></script>
</body>

</html>


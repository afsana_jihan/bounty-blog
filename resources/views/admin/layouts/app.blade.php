<!DOCTYPE html>
<html lang="en">
<title>Blog Dashboard</title>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <!--favicon-->
    <link rel="icon" href="{{ URL::asset('admin/images/favicon.ico') }}" type="image/x-icon">
    <!-- notifications css -->
    <link rel="stylesheet" href="{{ URL::asset('admin/plugins/notifications/css/lobibox.min.css') }}"/>
    <!-- Vector CSS -->
    <link href="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>
    <!-- simplebar CSS-->
    <link href="{{ URL::asset('admin/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{ URL::asset('admin/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{ URL::asset('admin/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{ URL::asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{ URL::asset('admin/css/sidebar-menu.css') }}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{ URL::asset('admin/css/app-style.css') }}" rel="stylesheet"/>

</head>

<body >

<!-- Start wrapper-->
<div id="wrapper">

    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
        <div class="brand-logo">
            <a href="#">
                <img src="{{ URL::asset('admin/images/logo-icon.png') }}" class="logo-icon" alt="logo icon">
                <h5 class="logo-text">BLOG</h5>
            </a>
        </div>

        <ul class="sidebar-menu do-nicescrol">
            <li class="sidebar-header">MAIN NAVIGATION</li>
            <li>
                <a href="index.html" class="waves-effect">
                    <i class="icon-home"></i><span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="javaScript:void();" class="waves-effect">
                    <i class="icon-handbag"></i><span>Category</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('category.index')}}"><i class="fa fa-long-arrow-right"></i>ALL Category</a></li>
                    <li><a href="{{route('category.create')}}"><i class="fa fa-long-arrow-right"></i>Add Category</a></li>
                </ul>
            </li>
            <li>
                <a href="javaScript:void();" class="waves-effect">
                    <i class="icon-handbag"></i><span>Post</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('post.allPostForAdmin')}}"><i class="fa fa-long-arrow-right"></i>ALL Post</a></li>
{{--                    <li><a href="{{route('post.create')}}"><i class="fa fa-long-arrow-right"></i>Add Post</a></li>--}}
                    <li><a href="{{route('post.index')}}"><i class="fa fa-long-arrow-right"></i>Approved Post</a></li>

                </ul>
            </li>

        </ul>

    </div>
    <!--End sidebar-wrapper-->

    <!--Start topbar header-->
    <header class="topbar-nav">
        <nav class="navbar navbar-expand fixed-top gradient-ibiza">
            <ul class="navbar-nav mr-auto align-items-center">
                <li class="nav-item">
                    <a class="nav-link toggle-menu" href="javascript:void();">
                        <i class="icon-menu menu-icon"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <form class="search-bar">
                        <input type="text" class="form-control" placeholder="Enter keywords">
                        <a href="javascript:void();"><i class="icon-magnifier"></i></a>
                    </form>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center right-nav-link">
                <li class="nav-item dropdown-lg">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                        <i class="icon-envelope-open"></i></a>
                    <div class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                You have 4 new messages
                                <span class="badge badge-danger">4</span>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="{{ URL::asset('admin/images/avatars/avatar-1.png') }}" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">Jhon Deo</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                            <small>Today, 4:10 PM</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="{{ URL::asset('admin/images/avatars/avatar-2.png') }}" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">Sara Jen</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                            <small>Yesterday, 8:30 AM</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="assets/images/avatars/avatar-3.png" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">Dannish Josh</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                            <small>5/11/2018, 2:50 PM</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <div class="avatar"><img class="align-self-start mr-3" src="assets/images/avatars/avatar-4.png" alt="user avatar"></div>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">Katrina Mccoy</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet.</p>
                                            <small>1/11/2018, 2:50 PM</small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item"><a href="javaScript:void();">See All Messages</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item dropdown-lg">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                        <i class="icon-bell"></i><span class="badge badge-primary badge-up">10</span></a>
                    <div class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                You have 10 Notifications
                                <span class="badge badge-primary">10</span>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <i class="icon-people fa-2x mr-3 text-info"></i>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">New Registered Users</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <i class="icon-cup fa-2x mr-3 text-warning"></i>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">New Received Orders</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item">
                                <a href="javaScript:void();">
                                    <div class="media">
                                        <i class="icon-bell fa-2x mr-3 text-danger"></i>
                                        <div class="media-body">
                                            <h6 class="mt-0 msg-title">New Updates</h6>
                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item"><a href="javaScript:void();">See All Notifications</a></li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                        <span class="user-profile"><img src="{{ URL::asset('admin/images/avatars/avatar-17.png')}}" class="img-circle" alt="user avatar"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <li class="dropdown-item user-details">
                            <a href="javaScript:void();">
                                <div class="media">
                                    <div class="avatar"><img class="align-self-start mr-3" src="{{ URL::asset('admin/images/avatars/avatar-17.png')}}" alt="user avatar"></div>
                                    <div class="media-body">
                                        <h6 class="mt-2 user-title"></h6>
                                        <p class="user-subtitle">{{ Auth::user()->name }}</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>

                        <li class="dropdown-item"><i class="icon-power mr-2"></i>
                            <a class="" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>



                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <div class="content-wrapper">
        @yield('content')

        <!-- End container-fluid-->

    </div><!--End content-wrapper-->
    <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

    <!--Start footer-->
    <footer class="footer">
        <div class="container">
            <div class="text-center">
                Copyright © 2018 Bounty Blog Admin
            </div>
        </div>
    </footer>
    <!--End footer-->

</div><!--End wrapper-->

<!-- Bootstrap core JavaScript-->
<script src="{{ URL::asset('admin/js/jquery.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/popper.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/bootstrap.min.js') }}"></script>

<!-- simplebar js -->
<script src="{{ URL::asset('admin/plugins/simplebar/js/simplebar.js') }}"></script>
<!-- waves effect js -->
<script src="{{ URL::asset('admin/js/waves.js') }}"></script>
<!-- sidebar-menu js -->
<script src="{{ URL::asset('admin/js/sidebar-menu.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ URL::asset('admin/js/app-script.js') }}"></script>

<!-- Vector map JavaScript -->
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- Sparkline JS -->
<script src="{{ URL::asset('admin/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
<!-- Chart js -->
<script src="{{ URL::asset('admin/plugins/Chart.js/Chart.min.js') }}"></script>
<!--notification js -->
<script src="{{ URL::asset('admin/plugins/notifications/js/lobibox.min.js') }}"></script>
<script src="{{ URL::asset('admin/plugins/notifications/js/notifications.min.js') }}"></script>
<!-- Index js -->
<script src="{{ URL::asset('admin/js/index.js') }}"></script>
@stack('scripts')


</body>

<!-- Mirrored from codervent.com/dashrock/color-admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Feb 2019 03:56:12 GMT -->
</html>

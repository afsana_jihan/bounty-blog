@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Add Post</h4>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('post.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="basic-input" class="col-sm-3 col-form-label">Post Title</label>
                                <div class="col-sm-9">
                                    <input type="text" id="basic-input"  name="post_title" class="form-control">
                                </div>
                            </div>
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <div class="form-group row">
                                <label for="basic-select" class="col-sm-3 col-form-label">Select Post Category</label>
                                <div class="col-sm-9">
                                    <select  class="form-control" name="category_id">
                                        @foreach($categories as $row)
                                            <option value="{{$row['category_id']}}">{{$row['category_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="basic-textarea" class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea rows="10" class="form-control" name="post_description" id="basic-textarea"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="basic-textarea" class="col-sm-3 col-form-label">Post Image</label>
                                <div class="col-sm-9">
                                <input type="file" name="path[]" multiple>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-offset-md-9 col-sm-3">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div>
    @endsection



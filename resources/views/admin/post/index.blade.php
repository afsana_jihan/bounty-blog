@extends('admin.layouts.app')
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"  />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{$message}}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-table"></i> All Post</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Sl No.</th>
                                    <th>Post Title</th>
                                    {{--<th>Parent Id</th>--}}
                                    <th>Post Category</th>
                                    <th>Post Description</th>
                                    <th>Approved Status</th>
                                    {{--<th>Slug</th>--}}
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $row)

                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$row->post_title}}</td>
                                        <td>{{$row->category->category_name}}</td>
                                        <td>{{$row->post_description}}</td>
                                        <td>

                   <!-- <input type="checkbox" name="approved_status" value="{{$row->id}}">  -->
 <!--  {{Form::checkbox('approved_status',1, $row->approved_status,array('id' => 'some-id'))}}  -->

                                           <!--  <input onclick="id()" id="toggle-class"  class="toggle-class"  type="checkbox" data-onstyle="success" data-offstyle="danger"data-toggle="toggle"> -->

                                          <input value="{{$row->approved_status}}" data-id="{{$row->post_id}}" type="checkbox" class="status_change" name="approved_status" {{ $row->approved_status==1 ? 'checked' : '' }} />

                                        </td>

                                        <td><a href="{{route('post.edit',$row->post_id)}}" class="btn btn-info">Edit</a></td>
                                        <td>
                                            <form method="POST" action="{{route('post.destroy',$row->post_id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

 @push("scripts")
<script>

    $(".status_change").click(function(){

    var post_id = $(this).data('id');
     // alert(post_id);
     $.ajax({
      type: "POST",

       dataType: "json",
      url: "{{route('post.changeStatus')}}",
      data: { 'post_id': post_id,'_token': "{{ csrf_token() }}"},
     success: function(data){
        alert(data.success);
       }
  });
    });

</script>

@endpush

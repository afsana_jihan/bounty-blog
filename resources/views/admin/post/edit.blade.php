@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Edit Post</h4>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{route('post.update', $post->post_id)}}">
                            @csrf
{{--                            @method('PUT')--}}
                            <input type="hidden" name="_method" value="PATCH" />
                            <div class="form-group row">
                                <label for="basic-input" class="col-sm-3 col-form-label">Post Title</label>
                                <div class="col-sm-9">
                                    <input type="text" id="basic-input"  name="post_title" value="{{$post->post_title}}" class="form-control">
                                </div>
                            </div>
{{--                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">--}}
                            <div class="form-group row">
                                <label for="basic-select" class="col-sm-3 col-form-label">Select Post Category</label>
                                <div class="col-sm-9">
                                    <select  class="form-control" name="category_id">
                                        @foreach($categories as $row)
                                            <option value="{{$row->category_id}}" @if($post->category_id==$row->category_id) selected @endif>{{$row->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="basic-textarea" class="col-sm-3 col-form-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea rows="10" class="form-control" name="post_description"  id="basic-textarea">{{$post->post_description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-offset-md-9 col-sm-3">
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div>
@endsection


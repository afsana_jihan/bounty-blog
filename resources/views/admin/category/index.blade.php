@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{$message}}</p>
                    </div>
                @endif
            </div>
        </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-table"></i> All Category</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Category Name</th>
                                {{--<th>Parent Id</th>--}}
                                <th>Main Category</th>
                                {{--<th>Slug</th>--}}
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $row)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$row->category_name}}</td>
                                {{--<td>{{$row->parent_id}}</td>--}}
                                <td>{{$row->parent ? $row->parent->category_name : "" }}</td>
                                {{--<td>{{$row->slug}}</td>--}}
                                <td><a href="{{route('category.edit',$row->category_id)}}" class="btn btn-info">Edit</a></td>
                                <td>
                                    <form method="POST" action="{{route('category.destroy',$row->category_id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection

@extends('admin.layouts.app')

@section('content')

    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">
                <h4 class="page-title">Add Category</h4>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{route('category.store')}}">
                            {{csrf_field()}}
                            <div class="form-group row">
                                <label for="basic-input" class="col-sm-3 col-form-label">Category Name</label>
                                <div class="col-sm-9">
                                    <input type="text" id="basic-input"  name="category_name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="basic-select" class="col-sm-3 col-form-label">Select Main Category</label>
                                <div class="col-sm-9">
                                    <select  class="form-control"   name="parent_id">
                                        <option value="">Please select an option if it is a sub category</option>
                                        @foreach($categories as $row)
                                            <option value="{{$row['category_id']}}">{{$row['category_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-offset-md-9 col-sm-3">
                                    <button type="submit" class="btn btn-primary">ADD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--End Row-->
    </div>

    @endsection
